from django.shortcuts import render, redirect
from .forms import FormSchedule
from .models import Schedule
from django.views.decorators.http import require_POST

# Create your views here.
def kegiatan(request):
	list_schedule = Schedule.objects.order_by('tanggal')
	form = FormSchedule()
	context = {'list_schedule' : list_schedule, 'form' : form}
	return render(request, 'story3v.html', context)

@require_POST
def tambah(request):
	form = FormSchedule(request.POST)
	if form.is_valid():
		newSchedule = Schedule(name=request.POST['name'], tanggal=request.POST['tanggal'], tempat=request.POST['tempat'],jam=request.POST['jam'],kategori=request.POST['kategori'])
		newSchedule.save()
	return redirect('homepage:kegiatan')

def pilih(request, kegiatan_id):
	kegiatan = Schedule.objects.get(pk= kegiatan_id)
	if kegiatan.pilih == False:
		kegiatan.pilih = True;
	else:
		kegiatan.pilih = False
	kegiatan.save()
	return redirect('homepage:kegiatan')
def hapusKegiatan(request):
	Schedule.objects.filter(pilih__exact = True).delete()

	return redirect('homepage:kegiatan')


def profile(request):
	return render(request, 'story3i.html')
def about(request):
	return render(request, 'story3ii.html')
def contact(request):
	return render(request, 'story3iii.html')
def photo(request):
	return render(request, 'story3iv.html')
