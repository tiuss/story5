from django import forms

class FormSchedule(forms.Form):
	name = forms.CharField(max_length = 60, widget=forms.TextInput(attrs={'class': 'form-control',
		   'placeholder':'ct : futsal', 'id' : 'nama'}))
	tanggal = forms.DateField(widget=forms.DateInput(attrs={'class': 'form-control',
			  'placeholder':'', 'type':'date'}))
	tempat = forms.CharField(max_length=60, widget=forms.TextInput(attrs={'class':'form-control',
		      'placeholder': 'ct : rumah hantu'}))
	jam = forms.TimeField(widget=forms.TimeInput(attrs={'class': 'form-control','type':'time'}))
	kategori = forms.CharField(max_length=150,
			widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'ct:tugas rumah'}))